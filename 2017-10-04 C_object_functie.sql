﻿DROP TABLE IF EXISTS bag_munge.c_object_functie;
CREATE TABLE bag_munge.c_object_functie AS ( --5:11 MM:SS 8 943 311 ROWS

SELECT bagobject.objectidentificatie,bagobject.hoofdadres,bagobject.oppervlakteverblijfsobject,bagobject.objectgeopunt,bagobject.pandidentificatie,bagobject.bouwjaar,MIN(verblijfsfunctieprioriteit) AS functie
FROM bag_munge.a_object_bouwjaar AS bagobject
INNER JOIN bagactueel.verblijfsobjectgebruiksdoelactueelbestaand AS functie ON functie.identificatie=bagobject.objectidentificatie
INNER JOIN bag_munge.functieprioriteit AS functieprio ON functieprio.verblijfsfunctie=functie.gebruiksdoelverblijfsobject::text
GROUP BY bagobject.objectidentificatie,bagobject.hoofdadres,bagobject.oppervlakteverblijfsobject,bagobject.objectgeopunt,bagobject.pandidentificatie,bagobject.bouwjaar

--LIMIT 10;
);

CREATE INDEX ON bag_munge.c_object_functie USING gist (objectgeopunt); --02:56 MM:SS
CREATE INDEX ON bag_munge.c_object_functie (objectidentificatie);
--VACUUM ANALYZE bag_munge.c_object_functie;

/*
SELECT COUNT(bagobject.objectidentificatie) AS aantal,COUNT(DISTINCT bagobject.objectidentificatie) AS uniek
FROM bag_munge.c_object_functie AS bagobject
*/