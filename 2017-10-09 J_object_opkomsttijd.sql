﻿DROP TABLE IF EXISTS bag_munge.j_object_opkomsttijd; -- 28:07 MM:SS
CREATE TABLE bag_munge.j_object_opkomsttijd AS (
SELECT ob.*,pgr.opkomsttijd,loc_naam,taakeenheid
FROM bag_munge.g_rembrand_categorie AS ob
CROSS JOIN LATERAL
	(
	SELECT *
	FROM opkomsttijden.hectopunt_snelste_opkomsttijd_taakeenheid_d as pgr
	WHERE ST_Buffer(objectgeopunt,500)&&geom -- bounding box voor snelheid?
	ORDER BY pgr.geom <-> ob.objectgeopunt
	LIMIT 1
	) AS pgr
--LIMIT 1000;
);

CREATE INDEX ON bag_munge.j_object_opkomsttijd USING gist (objectgeopunt); --19:51 MM:SS
CREATE INDEX ON bag_munge.j_object_opkomsttijd (objectidentificatie); -- 01:13 MM:SS
--VACUUM ANALYZE bag_munge.j_object_opkomsttijd;