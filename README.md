# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Complete SQL script om met open data de RemBrand object classificatie voor BAG objecten in Nederland te maken. De classificatie is letterlijk de modelbeschrijving van het project Gebiedsgebonden opkomsttijden van RemBrand. Het script voert dit letterlijk uit.

### How do I get set up? ###

* Bronbestanden binnenhalen
* Scripts stapsgewijs toepassen
* De scripts kunnen als SQL bestanden uitgevoerd worden of via het Python programma.
* De volgorde van SQL scripts is:
*	A_object_bouwjaar 	=> bepaalt per object één uniek pand en koppelt het bouwjaar
*	B_functie_prioriteit 	=> creeert een tabel om bij meerdere functies per pand één functie op basis van hoogste prioriteit te kiezen
*	C_object_functie 	=> koppelt per object één functie op basis van functieprioriteit uit tabel B
*	D_pand_woonfuncties	=> creeert een tabel met het aantal woonfuncties per pand en het aantal woonfuncties per pand met bouwjaar < 1975 tbv identificatie portiekflats
*	E_object_vierkant	=> koppelt elk object aan één CBS vierkant, met laagste CBS_id als tie-breaker indien pand precies op een grens ligt.
*	F_vierkant_aantal_objecten	=> telt op basis van de CBS vierkanten het aantal objecten en het aantal objecten in een pand met een bouwjaar van voor 1900 tbv de rembrand gebieden
*	G_RemBrand_classificatie	=> Bepaalt voor elk object de classificatie op basis van de voorgaand gegenereerde tabellen
*	H_object_hoogte		=> obsolete werd gebruikt om gebouwhoogte per object te bepalen, wordt niet meer gebruikt vanwege slechte datakwaliteit
*	I_object_regio		=> koppelt elk object aan een regiocode
*	J_object_opkomsttijd	=> bepaalt voor elk object de opkomsttijd en de eerste kazerne op basis van het aangeleverde rijtijdenmodel van HoltmanNet.nl
*	K_export_categorieen	=> koppelt de rembrand categorieen uit tabel G met de regio en opkomsttijd

### Contribution guidelines ###

* Verbeteringen in het proces zijn welkom. Om de inhoud te veranderen moet eerst de RemBrand modelbeschrijving aangepast worden.

### Who do I talk to? ###

* Vincent Oskam / FDS
