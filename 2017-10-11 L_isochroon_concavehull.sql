﻿DROP TABLE IF EXISTS bag_munge.l_concavehull;
CREATE TABLE bag_munge.l_concavehull AS (
SELECT loc_naam,ST_ConvexHull(ST_Collect(objectgeopunt)) as geom
--SELECT loc_naam,ST_ConcaveHull(ST_Union(objectgeopunt),0.50,false) as geom
FROM bag_munge.k_rembrand_totaal
WHERE vr_code = 16
AND opkomsttijd <=10
GROUP BY loc_naam
);

--select * from bag_munge.l_concavehull;