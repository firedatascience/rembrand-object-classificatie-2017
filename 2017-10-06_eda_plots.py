# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 11:28:18 2017

@author:    Vincent Oskam
@project:   RemBrand gebiedsgebonden opkomsttijden

@doel:      

"""

import os
import datetime as dt
import pandas as pd  # data frame operations
import numpy as np  # arrays and math functions
import matplotlib as mpl
import matplotlib.pyplot as plt  # 2D plotting
import seaborn as sns
import psycopg2
import getpass

pd.set_option('display.max_rows', 300)
pd.set_option('display.max_columns', 200)
pd.set_option('display.float_format', lambda x: '%.2f'%x)


os.chdir(r'D:\Projecten\2017\rembrand\2017-10 rembrand objectclassificatie')

# objecten = pd.read_csv('20171005_RemBrandClassificatie_v01.csv', sep=';')


p = getpass.getpass()


try:
    conn = psycopg2.connect("dbname='rembrand' user='postgres' host='localhost' password='" + p + "'")
except:
    print("I am unable to connect to the database")

try:
    connb = psycopg2.connect("dbname='brwzorgprofielen' user='postgres' host='localhost' password='" + p + "'")
except:
    print("I am unable to connect to the database")

sql = """
                SELECT objectidentificatie,rembrand_object_categorie,rembrand_object_categorie_reden,vr_code
                --,ST_X(objectgeopunt) AS rds_x,ST_Y(objectgeopunt) AS rds_y
                ,opkomsttijd, loc_naam
                ,CASE WHEN opkomsttijd <=10 THEN 'tien'
                WHEN opkomsttijd <=15 THEN 'vijftien'
                WHEN opkomsttijd <=18 THEN 'achttien'
                ELSE '> 18' END as opk_cat

                ,CASE WHEN rembrand_object_categorie = 1 AND opkomsttijd <=10 THEN 1
                WHEN rembrand_object_categorie = 1 AND opkomsttijd >10 THEN 0
                WHEN rembrand_object_categorie = 2 AND opkomsttijd <=15 THEN 1
                WHEN rembrand_object_categorie = 2 AND opkomsttijd >15 THEN 0
                WHEN rembrand_object_categorie = 3 AND opkomsttijd <=18 THEN 1
                WHEN rembrand_object_categorie = 3 AND opkomsttijd >18 THEN 0
                END AS binnen_objectnorm
                FROM bag_munge.k_rembrand_totaal;
    
      """

sqlb = """
                SELECT objectidentificatie,rembrand_object_categorie,rembrand_object_categorie_reden,vr_code
                --,ST_X(objectgeopunt) AS rds_x,ST_Y(objectgeopunt) AS rds_y
                ,opkomsttijd, loc_naam
                ,CASE WHEN opkomsttijd <=10 THEN 'tien'
                WHEN opkomsttijd <=15 THEN 'vijftien'
                WHEN opkomsttijd <=18 THEN 'achttien'
                ELSE '> 18' END as opk_cat

                ,CASE WHEN rembrand_object_categorie = 1 AND opkomsttijd <=10 THEN 1
                WHEN rembrand_object_categorie = 1 AND opkomsttijd >10 THEN 0
                WHEN rembrand_object_categorie = 2 AND opkomsttijd <=15 THEN 1
                WHEN rembrand_object_categorie = 2 AND opkomsttijd >15 THEN 0
                WHEN rembrand_object_categorie = 3 AND opkomsttijd <=18 THEN 1
                WHEN rembrand_object_categorie = 3 AND opkomsttijd >18 THEN 0
                END AS binnen_objectnorm
                FROM bag_munge.k_rembrand_totaal_varb;
    
      """



del objecten

objecten = pd.read_sql_query(sql, conn) # start: 13:23 HH:SS duur: 01:30 MM:SS

del objectenb
objectenb = pd.read_sql_query(sqlb, conn) # start: 13:23 HH:SS duur: 01:30 MM:SS

vara = objecten.groupby(['vr_code','rembrand_object_categorie'])['binnen_objectnorm'].describe().unstack()['mean']

# objecten.groupby(['vr_code','rembrand_object_categorie_reden'])['binnen_objectnorm'].describe().unstack()['mean']

varb = objectenb[objectenb['rembrand_object_categorie']==1].groupby(['vr_code','rembrand_object_categorie_reden'])['binnen_objectnorm'].describe().unstack()['mean']

objecten.columns

objecten.groupby(['vr_code','rembrand_object_categorie'])['vr_code'].count().unstack()

objecten[objecten['rembrand_object_categorie']==1].groupby(['vr_code','rembrand_object_categorie_reden'])['vr_code'].count().unstack()
objecten[objecten['rembrand_object_categorie']==1].groupby(['vr_code','rembrand_object_categorie_reden'])['binnen_objectnorm'].mean().unstack()
objecten.groupby(['vr_code','rembrand_object_categorie'])['binnen_objectnorm'].mean().unstack()
objecten[objecten['rembrand_object_categorie']==1].groupby(['vr_code','rembrand_object_categorie_reden'])['binnen_objectnorm'].sum().unstack()
objecten[objecten['rembrand_object_categorie']==1].groupby(['vr_code','rembrand_object_categorie_reden'])['binnen_objectnorm'].size().unstack()

# variant b
objectenb.groupby(['vr_code','rembrand_object_categorie'])['vr_code'].count().unstack()

objectenb[objectenb['rembrand_object_categorie']==1].groupby(['vr_code','rembrand_object_categorie_reden'])['vr_code'].count().unstack()
objectenb[objectenb['rembrand_object_categorie']==1].groupby(['vr_code','rembrand_object_categorie_reden'])['binnen_objectnorm'].mean().unstack()
objectenb.groupby(['vr_code','rembrand_object_categorie'])['binnen_objectnorm'].mean().unstack()
objectenb[objectenb['rembrand_object_categorie']==1].groupby(['vr_code','rembrand_object_categorie_reden'])['binnen_objectnorm'].sum().unstack()
objectenb[objectenb['rembrand_object_categorie']==1].groupby(['vr_code','rembrand_object_categorie_reden'])['binnen_objectnorm'].size().unstack()


pd.crosstab(objecten['opk_cat'],objecten['rembrand_object_categorie'],normalize=True)

objecten.groupby('rembrand_object_categorie')['opk_cat'].mean()

for i in range(0,26):
    print(i)
    print(objecten[objecten['vr_code']==i].groupby('rembrand_object_categorie')['opk_cat'].describe())
    print(pd.crosstab(objecten[objecten['vr_code']==i]['binnen_objectnorm'],objecten[objecten['vr_code']==i]['rembrand_object_categorie'],normalize=True))
    

    print((objecten[objecten['vr_code']==i].groupby('rembrand_object_categorie')['rembrand_object_categorie'].count()/objecten[objecten['vr_code']==i]['rembrand_object_categorie'].count()))
    
    print(objecten[objecten['vr_code']==i]['rembrand_object_categorie'].value_counts(normalize=True))
    print(objecten[objecten['vr_code']==i]['rembrand_object_categorie'].value_counts(normalize=False))
    

sns.barplot(data=objecten,y='rembrand_object_categorie')




if 1==1:
    plt.figure(figsize=(8, 10))
    plot = sns.boxplot(data=objecten,y='vr_code',hue='rembrand_object_categorie',x='opkomsttijd',orient='h')
    plot.set(xlim=(0,30))
    sns.plt.show()



if 1==1:
    g = sns.FacetGrid(data=objecten, size=4, row='rembrand_object_categorie',col='vr_code', margin_titles=True, palette='husl', sharex=True)
    g.map(sns.boxplot, 'opkomsttijd')
    g.set(xlim=(0, 30))
    g.axes[0][0].legend(title="weekend")
    g.set_xlabels("uur")
    plt.subplots_adjust(top=0.85)
    sns.plt.suptitle('sted')
    sns.plt.show()   


if 1==1:
    plt.figure(figsize=(8, 10))
    g = sns.FacetGrid(data=objecten, col='rembrand_object_categorie',row='vr_code', margin_titles=True, palette='husl', sharex=True)
    g.map(sns.countplot, 'binnen_objectnorm')
    
if 1==1:
    plt.figure(figsize=(8, 10))
    g = sns.FacetGrid(data=objecten[objecten['rembrand_object_categorie']==1], size=4, col='rembrand_object_categorie_reden',row='vr_code', margin_titles=True, palette='husl', sharex=True)
    g.map(sns.factorplot, 'binnen_objectnorm')
    
    
if 1==1:
    g = sns.FacetGrid(data=e, size=5, aspect=0.8, col='vr', hue='opkt_nr', margin_titles=True, palette='husl', sharex=True)
    g.map(sns.kdeplot, 'opkt', cumulative=True) #orient='h', hue=  
    g.set(xlim=(0, 15))
    g.axes[0][0].legend(title="taakeenheden")
    # g.add_legend('taakeenheden')    

if 1==1:
    g = sns.factorplot(x="rembrand_object_categorie", y="binnen_objectnorm", col="vr_code",
                       data=objecten, saturation=.5,
                       kind="bar", ci=None, aspect=1, col_wrap=5)
    
if 1==1:
    g = sns.factorplot(x="rembrand_object_categorie", hue="binnen_objectnorm", col="vr_code",
                       data=objecten[objecten['rembrand_object_categorie']==1], saturation=.5,
                       kind="count", ci=None, aspect=1, col_wrap=5)
    
if 1==1:
    g = sns.factorplot(y="rembrand_object_categorie_reden", hue="binnen_objectnorm", col="vr_code",
                       data=objecten[objecten['rembrand_object_categorie']==1], saturation=.5,
                       kind="count", ci=None, aspect=1.5, col_wrap=5, sharex=True,sharey=True)
#    ax = g.fig.get_axes()
#    ax.set(yscale="log")
    for i in range(0,6):
        for j in range(0,6):
            print(i,j)
    g.fig.get_axes()[0].set_xscale('log')
    plt.show()


(g.set_axis_labels("", "Survival Rate")
  .set_xticklabels(["Men", "Women", "Children"])
  .set_titles("{col_name} {col_var}")
  .set(ylim=(0, 1))
  .despine(left=True))  


















