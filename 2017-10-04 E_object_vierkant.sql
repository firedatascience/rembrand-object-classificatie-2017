﻿DROP TABLE IF EXISTS bag_munge.e_object_vierkant ; --13:29 MM:SS 8 943 311 ROWS
CREATE TABLE bag_munge.e_object_vierkant AS (
SELECT bagobject.objectgeopunt,bagobject.objectidentificatie,bagobject.pandidentificatie,MIN(vierkant."c28992r500") AS "c28992r500"
FROM bag_munge.c_object_functie AS bagobject
LEFT JOIN public.cbsvierkant AS vierkant ON ST_Intersects(bagobject.objectgeopunt,geom) --ST_Within(bagobject.objectgeopunt,geom) 
WHERE geom && bagobject.objectgeopunt -- Zorgt er voor de query sneller gaat 
GROUP BY bagobject.objectgeopunt,bagobject.objectidentificatie,bagobject.pandidentificatie
--LIMIT 10;
);

CREATE INDEX ON bag_munge.e_object_vierkant (objectidentificatie);
--VACUUM ANALYZE bag_munge.e_object_vierkant;

SELECT COUNT(bagobject.objectidentificatie) AS aantal,COUNT(DISTINCT bagobject.objectidentificatie) AS uniek
FROM bag_munge.e_object_vierkant AS bagobject

/*
SELECT COUNT(bagobject.objectidentificatie),COUNT(DISTINCT bagobject.objectidentificatie) --bagobject.objectidentificatie,bagobject.pandidentificatie,vierkant."c28992r500"
FROM bag_munge.c_object_functie AS bagobject
INNER JOIN public.cbsvierkant AS vierkant ON ST_Intersects(bagobject.objectgeopunt,geom) 
LIMIT 10;
*/




