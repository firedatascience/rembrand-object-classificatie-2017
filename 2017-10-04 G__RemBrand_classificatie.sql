﻿DROP TABLE IF EXISTS bag_munge.g_rembrand_categorie;
CREATE TABLE bag_munge.g_rembrand_categorie AS ( --14:10 MM:SS --8 943 311 
SELECT bagobject.*
--COUNT(bagobject.objectidentificatie)
,CASE
	WHEN portiek.woonfuncties_1975 >= 8 THEN 'portiek'
--	WHEN hoogte.maximum_hoogte-hoogte.minimum_hoogte > 25 AND bagobject.functie = 'C.    woonfunctie' THEN '> 25 m.'
	WHEN bagobject.functie = 'A.    celfunctie' THEN 'celfunctie'
	WHEN bagobject.functie = 'B.    gezondheidszorgfunctie' THEN 'gezondheidszorg'
	WHEN vierkantaantal.aantal_objecten_1900 >= 80 THEN '>= 80 objecten voor 1900'
	WHEN vierkantaantal.aantal_objecten > 50 THEN 'geen'
	ELSE '<= 50 objecten'
END AS RemBrand_object_categorie_reden

,CASE
	WHEN portiek.woonfuncties_1975 >= 8 THEN 1
--	WHEN hoogte.maximum_hoogte-hoogte.minimum_hoogte > 25 AND bagobject.functie = 'C.    woonfunctie' THEN 1
	WHEN bagobject.functie = 'A.    celfunctie' THEN 1
	WHEN bagobject.functie = 'B.    gezondheidszorgfunctie' THEN 1
	WHEN vierkantaantal.aantal_objecten_1900 >= 80 THEN 1
	WHEN vierkantaantal.aantal_objecten > 50 THEN 2
	ELSE 3
END AS RemBrand_object_categorie


FROM bag_munge.c_object_functie as bagobject

LEFT JOIN bag_munge.d_pand_woonfuncties AS portiek ON portiek.pandidentificatie=bagobject.pandidentificatie
LEFT JOIN bag_munge.e_object_vierkant AS vierkant ON vierkant.objectidentificatie=bagobject.objectidentificatie
LEFT JOIN bag_munge.f_vierkant_aantallen AS vierkantaantal ON vierkantaantal."c28992r500"=vierkant."c28992r500"
--LEFT JOIN bag_munge.h_object_hoogte AS hoogte ON hoogte.objectidentificatie = bagobject.objectidentificatie
--LIMIT 10;
);

CREATE INDEX ON bag_munge.g_rembrand_categorie USING gist (objectgeopunt); --08:56 MM:SS
CREATE INDEX ON bag_munge.g_rembrand_categorie (objectidentificatie);
--VACUUM ANALYZE bag_munge.g_rembrand_categorie;


DROP TABLE IF EXISTS bag_munge.g_rembrand_categorie_varb;
CREATE TABLE bag_munge.g_rembrand_categorie_varb AS ( --14:10 MM:SS --8 943 311 || 10-10-2017 26:30 8 943 325
SELECT 
bagobject.*
--COUNT(bagobject.objectidentificatie)
,CASE
	WHEN portiek.woonfuncties_1975 >= 8 THEN 'portiek'
	WHEN hoogte.maximum_hoogte-hoogte.minimum_hoogte > 25 
		AND bagobject.functie = 'C.    woonfunctie' THEN '> 25 m.'
	WHEN bagobject.functie = 'A.    celfunctie' THEN 'celfunctie'
	WHEN bagobject.functie = 'B.    gezondheidszorgfunctie' THEN 'gezondheidszorg'
	WHEN vierkantaantal.aantal_objecten_1900 >= 80 THEN '>= 80 objecten voor 1900'
	WHEN vierkantaantal.aantal_objecten > 50 THEN 'geen'
	ELSE '<= 50 objecten'
END AS RemBrand_object_categorie_reden

,CASE
	WHEN portiek.woonfuncties_1975 >= 8 THEN 1
	WHEN hoogte.maximum_hoogte-hoogte.minimum_hoogte > 25 
		AND bagobject.functie = 'C.    woonfunctie' THEN 1
	WHEN bagobject.functie = 'A.    celfunctie' THEN 1
	WHEN bagobject.functie = 'B.    gezondheidszorgfunctie' THEN 1
	WHEN vierkantaantal.aantal_objecten_1900 >= 80 THEN 1
	WHEN vierkantaantal.aantal_objecten > 50 THEN 2
	ELSE 3
END AS RemBrand_object_categorie


FROM bag_munge.c_object_functie as bagobject

LEFT JOIN bag_munge.d_pand_woonfuncties AS portiek ON portiek.pandidentificatie=bagobject.pandidentificatie
LEFT JOIN bag_munge.e_object_vierkant AS vierkant ON vierkant.objectidentificatie=bagobject.objectidentificatie
LEFT JOIN bag_munge.f_vierkant_aantallen AS vierkantaantal ON vierkantaantal."c28992r500"=vierkant."c28992r500"
LEFT JOIN bag_munge.h_object_hoogte AS hoogte ON hoogte.objectidentificatie = bagobject.objectidentificatie
--LIMIT 10;
);

CREATE INDEX ON bag_munge.g_rembrand_categorie_varb USING gist (objectgeopunt); -- 08:56 MM:SS
CREATE INDEX ON bag_munge.g_rembrand_categorie_varb (objectidentificatie); -- 01:40 MM:SS
--VACUUM ANALYZE bag_munge.g_rembrand_categorie_varb; -- 00:01 MM:SS


-- variatie zonder hoogte
--CREATE TABLE bag_munge.g_rembrand_categorie_varb AS ( --6:10 MM:SS --8 943 311 

/*
--LIMIT 10;SELECT COUNT(objectidentificatie),COUNT(DISTINCT objectidentificatie)
FROM bag_munge.a_object_bouwjaar
LIMIT 10;

SELECT COUNT(identificatie),COUNT(DISTINCT identificatie)
FROM bagactueel.verblijfsobjectactueelbestaand

SELECT COUNT(objectidentificatie),COUNT(DISTINCT objectidentificatie)
FROM bag_munge.c_object_functie
LIMIT 10;

SELECT COUNT(pandidentificatie),COUNT(DISTINCT pandidentificatie)
FROM bag_munge.d_pand_woonfuncties
LIMIT 10;

SELECT COUNT(objectidentificatie),COUNT(DISTINCT objectidentificatie)
FROM bag_munge.e_object_vierkant
LIMIT 10;*/