﻿DROP TABLE IF EXISTS bag_munge.d_pand_woonfuncties ;
CREATE TABLE bag_munge.d_pand_woonfuncties AS ( --00:52 MM:SS
SELECT pandidentificatie,
COUNT(objectidentificatie) FILTER (WHERE functie = 'C.    woonfunctie' ) AS woonfuncties
,COUNT(objectidentificatie) FILTER (WHERE functie = 'C.    woonfunctie' AND bouwjaar < 1975 ) AS woonfuncties_1975
FROM bag_munge.c_object_functie
GROUP BY pandidentificatie
--LIMIT 10;
);

SELECT *
FROM bag_munge.d_pand_woonfuncties
LIMIT 10;