﻿CREATE TABLE bag_munge.i_object_regio AS ( -- 58:40 MM:SS
                SELECT objectidentificatie::text,pandidentificatie::text,vrs.tdn_code AS vr_code
                FROM bag_munge.g_rembrand_categorie as bagobject
                LEFT JOIN public.vrs AS vrs ON ST_intersects(bagobject.objectgeopunt,vrs.geom)
                where vrs.geom && bagobject.objectgeopunt -- voegt snelheid toe (mogelijk met gist indexen op beide tabellen nog meer)
        );