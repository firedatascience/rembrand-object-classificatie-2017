﻿DROP TABLE IF EXISTS bag_munge.a_object_bouwjaar;
CREATE TABLE bag_munge.a_object_bouwjaar AS ( --04:15 MM:SS

/*
a_object_bouwjaar 

Koppel actueel bestaand object aan actueel bestaande panden, selecteert unieke objecten met het laagste bouwjaar voor het geval er meerdere panden per object bestaan.
*/


SELECT bagobject.identificatie AS objectidentificatie,bagobject.hoofdadres,bagobject.oppervlakteverblijfsobject,bagobject.geopunt AS objectgeopunt,MIN(pand.identificatie) AS pandidentificatie,MIN(pand.bouwjaar) AS bouwjaar
FROM bagactueel.verblijfsobjectactueelbestaand AS bagobject
INNER JOIN bagactueel.verblijfsobjectpandactueelbestaand AS objectpand ON objectpand.identificatie = bagobject.identificatie
INNER JOIN bagactueel.pandactueelbestaand AS pand ON pand.identificatie = objectpand.gerelateerdpand
GROUP BY bagobject.identificatie, bagobject.hoofdadres,bagobject.oppervlakteverblijfsobject,bagobject.geopunt
--LIMIT 10;
);

CREATE INDEX ON bag_munge.a_object_bouwjaar (objectidentificatie);
VACUUM bag_munge.a_object_bouwjaar;
CREATE INDEX ON bag_munge.a_object_bouwjaar (hoofdadres);
CREATE INDEX ON bag_munge.a_object_bouwjaar (pandidentificatie);
CREATE INDEX ON bag_munge.a_object_bouwjaar (pandidentificatie);
CREATE INDEX ON bag_munge.a_object_bouwjaar (pandidentificatie);


