﻿DROP TABLE IF EXISTS bag_munge.functieprioriteit;
CREATE TABLE bag_munge.functieprioriteit (
verblijfsfunctie text
,verblijfsfunctieprioriteit text
);

INSERT INTO bag_munge.functieprioriteit(
            verblijfsfunctie, verblijfsfunctieprioriteit)
    VALUES ('celfunctie','A.    celfunctie'),
('gezondheidszorgfunctie','B.    gezondheidszorgfunctie'),
('woonfunctie','C.    woonfunctie'),
('logiesfunctie','D.    logiesfunctie'),
('onderwijsfunctie','E.    onderwijsfunctie'),
('winkelfunctie','F.    winkelfunctie'),
('industriefunctie','G.    industriefunctie'),
('kantoorfunctie','H.    kantoorfunctie'),
('bijeenkomstfunctie','I.    bijeenkomstfunctie'),
('sportfunctie','J.  sportfunctie'),
('overige gebruiksfunctie','K.  overige gebruiksfunctie');

SELECT *
FROM bag_munge.functieprioriteit
	
SELECT DISTINCT gebruiksdoelverblijfsobject
FROM bagactueel.verblijfsobjectgebruiksdoelactueelbestaand