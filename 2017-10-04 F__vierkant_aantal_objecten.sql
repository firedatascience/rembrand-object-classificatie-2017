﻿DROP TABLE IF EXISTS bag_munge.f_vierkant_aantallen;
CREATE TABLE bag_munge.f_vierkant_aantallen AS ( -- 00:23 MM:SS 94 750 ROWS (10:41 zonder index op objectidentificatie) 
SELECT "c28992r500"
,COUNT(bagobject.objectidentificatie) AS aantal_objecten
,COUNT(bagobject.objectidentificatie) FILTER (WHERE bouwjaar < 1900)  AS aantal_objecten_1900
FROM bag_munge.e_object_vierkant AS vierkant
LEFT JOIN bag_munge.c_object_functie AS bagobject ON bagobject.objectidentificatie=vierkant.objectidentificatie
GROUP BY "c28992r500"
--LIMIT 10;
);

SELECT COUNT(c28992r500),COUNT(DISTINCT c28992r500)
FROM bag_munge.f_vierkant_aantallen
LIMIT 10;