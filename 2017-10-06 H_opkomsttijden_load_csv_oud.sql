﻿CREATE SCHEMA opkomsttijd;
DROP TABLE IF EXISTS opkomsttijd.opk4;

CREATE TABLE rembrand.opk_tke6(

gerelateerdpand numeric
,opkt_1 numeric
,loc_1 text
    );
    
    
COPY rembrand.opk_tke6
FROM 'D:\Projecten\2017\rembrand\opk\bag_tke6.csv' DELIMITER ';' NULL '' CSV HEADER
;

CREATE INDEX opk_tke6_pand ON rembrand.opk_tke6 (gerelateerdpand);

-- VACUUM ANALYZE rembrand.opk_tke6

-- SELECT * FROM rembrand.opk_tke6 LIMIT 10;