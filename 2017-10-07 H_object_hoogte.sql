﻿CREATE TABLE bag_munge.h_object_hoogte AS ( --   MM:SS
SELECT a.objectidentificatie,a.pandidentificatie,b.maximum_hoogte,b.minimum_hoogte
FROM bag_munge.c_object_functie AS a
INNER JOIN public.gebouw AS b ON ST_Intersects(a.objectgeopunt,b.geom)
--LIMIT 10;
);
CREATE INDEX ON bag_munge.h_object_hoogte (objectidentificatie);