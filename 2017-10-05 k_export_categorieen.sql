-- aanpassing om mogelijk extra uur weg te halen ﻿

--   MM:SS
DROP TABLE IF EXISTS bag_munge.k_rembrand_totaal;
CREATE TABLE bag_munge.k_rembrand_totaal AS( -- 07:00 MM:SS
WITH x (
SELECT bagobject.objectidentificatie
--,hoofdadres,functie,pandidentificatie,bouwjaar,oppervlakteverblijfsobject
,bagobject.rembrand_object_categorie,bagobject.rembrand_object_categorie_reden
,regio.vr_code
,bagobject.objectgeopunt
/* Om onduidelijke reden duurt het volgende blok meer dan een uur...*/
-- 		,CASE WHEN opkomsttijd <=10 THEN 'tien'
--                 WHEN opkomsttijd <=15 THEN 'vijftien'
--                 WHEN opkomsttijd <=18 THEN 'achttien'
--                 ELSE '> 18' END as opk_cat
-- 
--                 ,CASE WHEN bagobject.rembrand_object_categorie = 1 AND opkomsttijd <=10 THEN 1
--                 WHEN bagobject.rembrand_object_categorie = 1 AND opkomsttijd >10 THEN 0
--                 WHEN bagobject.rembrand_object_categorie = 2 AND opkomsttijd <=15 THEN 1
--                 WHEN bagobject.rembrand_object_categorie = 2 AND opkomsttijd >15 THEN 0
--                 WHEN bagobject.rembrand_object_categorie = 3 AND opkomsttijd <=18 THEN 1
--                 WHEN bagobject.rembrand_object_categorie = 3 AND opkomsttijd >18 THEN 0
--                 END AS binnen_objectnorm
--,ST_X(bagobject.objectgeopunt) AS rds_x,ST_Y(bagobject.objectgeopunt) AS rds_y
,opk.opkomsttijd,opk.loc_naam
FROM bag_munge.g_rembrand_categorie AS bagobject
LEFT JOIN bag_munge.i_object_regio AS regio ON regio.objectidentificatie=bagobject.objectidentificatie::text
LEFT JOIN bag_munge.j_object_opkomsttijd AS opk ON opk.objectidentificatie=bagobject.objectidentificatie
)

select *, 
CASE WHEN opkomsttijd <=10 THEN 'tien'
                 WHEN opkomsttijd <=15 THEN 'vijftien'
                 WHEN opkomsttijd <=18 THEN 'achttien'
                 ELSE '> 18' END as opk_cat
 
                 ,CASE WHEN x.rembrand_object_categorie = 1 AND opkomsttijd <=10 THEN 1
                 WHEN x.rembrand_object_categorie = 1 AND opkomsttijd >10 THEN 0
                 WHEN x.rembrand_object_categorie = 2 AND opkomsttijd <=15 THEN 1
                 WHEN x.rembrand_object_categorie = 2 AND opkomsttijd >15 THEN 0
                 WHEN x.rembrand_object_categorie = 3 AND opkomsttijd <=18 THEN 1
                 WHEN x.rembrand_object_categorie = 3 AND opkomsttijd >18 THEN 0
                 END AS binnen_objectnorm
,ST_X(x.objectgeopunt) AS rds_x,ST_Y(x.objectgeopunt) AS rds_y

--LIMIT 10;
);

CREATE INDEX ON bag_munge.k_rembrand_totaal USING gist (objectgeopunt); -- 11:55 MM:SS
CREATE INDEX ON bag_munge.k_rembrand_totaal (objectidentificatie); --00:47 MM:SS
--VACUUM ANALYZE bag_munge.k_rembrand_totaal;


--EXPORT AS CSV  MM:SS
/*
SELECT objectidentificatie,rembrand_object_categorie,rembrand_object_categorie_reden,vr_code
,ST_X(objectgeopunt) AS rds_x,ST_Y(objectgeopunt) AS rds_y
,opkomsttijd,loc_naam
FROM bag_munge.k_rembrand_totaal
--WHERE vr_code = 16

*/




